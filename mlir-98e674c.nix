# Nov 28, 2023
{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz";
    sha256="sha256:1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  }) {}
}: import ./build-mlir.nix {
    inherit pkgs;
    rev =  "98e674c9f16d677d95c67bc130e267fae331e43c";
    sha256 = "sha256-TfZitIisZJzrxD031G25VsTENk3oesLsR1uu67jsIIY=";
}
