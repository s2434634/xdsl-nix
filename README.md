# MLIR nix setup

* Builds MLIR
* Attempts to create a shell identical to the user's shell

## Usage

Run `nix-shell` in this directory, or `nix-shell /path/to/this/dir/shell.nix`
from any other directory. Running this for the first time will likely take about
an hour, as it will need to build MLIR, subsequent runs will be under a second.

By default, `nix-shell` will attempt to recreate the user's current shell,
obtained from the `SHELL` environment variable. To control which shell to use,
set this variable in advance: `SHELL=/bin/bash nix-shell`.

`nix-shell` will also attempt to set a prefix for the prompt using
`PROMPT_COMMAND` (bash only), to avoid this use
`nix-shell --arg prompt false`.

To use nix's default shell instead of `SHELL`, use
`nix-shell --arg shell false`.

## Versions

This script supports multiple versions of MLIR.

You can use the argument `--argstr mlir-ver "VERSION"` to `nix-shell` to set
package versions.

* `mlir-d401987` (default)
* `98e674c`
* `a3f2751`
* `98d8b68`

### Adding a new version

There is a chance you can just copy one of the `PKG-VERSION.nix` files, then add
it to `shell.nix` and run `nix-shell --argstr PKG-ver "NEW_VERSION"`, but it may
not work if the process for building the packages has changed between different
versions.
