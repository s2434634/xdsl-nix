# Aug 14, 2023
{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz";
    sha256="sha256:1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  }) {}
}: import ./build-mlir.nix {
    inherit pkgs;
    rev =  "a3f2751f782f3cdc6ba4790488ec20163a40ac37";
    sha256 = "sha256-qgzwKZk/6KnFtN9zvYOlejjy0bxvtVDG0+tDozI5bAU=";
}
