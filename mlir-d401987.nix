# Oct 1, 2024, xDSL recomended version
{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz";
    sha256="sha256:1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  }) {}
}: import ./build-mlir.nix {
    inherit pkgs;
    rev =  "d401987fe349a87c53fe25829215b080b70c0c1a";
    sha256 = "sha256-xTo44+vH1Bz49fEl4bIpU3eIgQtFuBTGbgU7mzeY33s=";
}
