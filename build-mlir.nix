{
    pkgs,
    rev,
    sha256 ? "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="
} : pkgs.stdenv.mkDerivation {
  LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib/";
  pname = "mlir";
  version = rev;

  src = [
    (pkgs.fetchgit {
        inherit sha256;
        url = "https://github.com/llvm/llvm-project.git";
        rev = rev;
    })
  ];

  buildInputs = [
    pkgs.cmake
    pkgs.gcc
    pkgs.ninja
    pkgs.clang
    pkgs.libcxx
    pkgs.lld
    pkgs.python311
    pkgs.git
  ];

  configurePhase = ''
  cmake llvm                            \
    -B build                            \
    -G Ninja                            \
    -DCMAKE_BUILD_TYPE=Release          \
    -DCMAKE_CXX_COMPILER=clang++        \
    -DCMAKE_C_COMPILER=clang            \
    -DLLVM_BUILD_EXAMPLES=OFF           \
    -DLLVM_ENABLE_ASSERTIONS=ON         \
    -DLLVM_ENABLE_LLD=ON                \
    -DLLVM_ENABLE_PROJECTS="mlir"       \
    -DLLVM_LIT_ARGS=-v                  \
    -DLLVM_TARGETS_TO_BUILD=host        \
    --install-prefix "$out"
  '';

  buildPhase = ''
  cmake --build build -- --verbose
  '';

  installPhase = ''
  cmake --install build
  '';
}
