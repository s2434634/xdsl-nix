# Nov 8, 2023
{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz";
    sha256="sha256:1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  }) {}
}: import ./build-mlir.nix {
    inherit pkgs;
    rev = "98d8b688bd351850ca99d3e5e9dfdf582c83c4f4";
    sha256 = "sha256-xraQLh3Xo2xWzW+vjSUuYSpyDO0PO1ATyk0U1pkIDFM=";
}
