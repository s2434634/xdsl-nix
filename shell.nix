{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz";
    sha256="sha256:1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  }) {},
  shell    ? true,
  prompt   ? true,
  mlir-ver ? "d401987",
}:
let
    mlir_path = {
      "a3f2751" = ./mlir-a3f2751.nix;
      "98d8b68" = ./mlir-98d8b68.nix;
      "98e674c" = ./mlir-98e674c.nix;
      "d401987" = ./mlir-d401987.nix;
    }."${mlir-ver}" or (abort "
      Valid values for mlir-ver are:
        d401987 # default
        98e674c
        a3f2751
        98d8b68");
in
pkgs.mkShell ({
    LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib/";
    buildInputs = [
        (import mlir_path {})
    ] ;

    shellHook = ''
    if [ -n "$OUTER_SHELL" ]; then
        $OUTER_SHELL
        exit
    fi
    '';
} // pkgs.lib.optionalAttrs shell { OUTER_SHELL = builtins.getEnv "SHELL"; }
  // pkgs.lib.optionalAttrs (shell && prompt) { PROMPT_COMMAND = "printf '(nix) '"; }
)
